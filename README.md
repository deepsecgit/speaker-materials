# Speaker Materials

This repository holds templates, graphics, and other useful files for DeepSec & DeepINTEL conference speakers. Everything you need to create presentations for our events can be found here.